package com.example.demo.first;

import org.springframework.stereotype.Service;

//НАС НЕ БЕСПОКИТ! ПРОСТО ДЛЯ ПРИМЕРА!
@Service
public class ThreadLocalService {

    public void doSomething() throws RuntimeException {
        //some logic
        someMethodNeedRequestId();
        someMethodNeedUncId();
        //some logic
    }

    //просто метод 1 для примера.
    public void someMethodNeedRequestId() throws RuntimeException {
        Headers headers = (Headers) ThreadLocalCustom.get();
        String requestId = headers.getRequestId();
        //Some Logic
    }

    //просто метод 2 для примера.
    public void someMethodNeedUncId() throws RuntimeException {
        Headers headers = (Headers) ThreadLocalCustom.get();
        String uncId = headers.getUncId();
        //Some Logic
    }
}
