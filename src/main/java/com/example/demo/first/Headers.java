package com.example.demo.first;

import lombok.AllArgsConstructor;
import lombok.Data;

//НАС НЕ БЕСПОКИТ! ПРОСТО ДЛЯ ПРИМЕРА!
@Data
@AllArgsConstructor
public class Headers {
    private String userId;
    private String requestId;
    private String uncId;
    //...

}
