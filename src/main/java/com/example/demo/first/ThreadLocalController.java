package com.example.demo.first;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@Slf4j
@Controller
@RequiredArgsConstructor
public class ThreadLocalController {

    private final ThreadLocalService threadLocalService;

    @GetMapping(path = "/someMethod")
    public ResponseEntity<Void> someMethod(@RequestHeader("uncId") String uncId,
                                           @RequestHeader("requestId") String requestId,
                                           @RequestHeader("userId") String userId
                                           //...
    ) {

        Headers headers = new Headers(userId, requestId, uncId);
        ThreadLocalCustom.set(headers);
        try {
            threadLocalService.doSomething();
        } catch (RuntimeException e) {
            log.info("some exception ", e);
        }

        return ResponseEntity.ok().build();
    }
}

/**
 * могут быть ошибки и наша мапа не очиститься, необходимо сделать так чтобы очистка проходила ВСЕГДА!
 * <p>
 * try {
 * threadLocalService.doSomething();
 * }catch (RuntimeException e) {
 * log.info("some exception ", e);
 * } finally {
 * threadLocalService.clear();
 * }
 */
