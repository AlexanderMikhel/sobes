package com.example.demo.first;

import java.util.HashMap;
import java.util.Map;

public class ThreadLocalCustom {

    private static Map<Long, Object> threadLocalMap = new HashMap<>();

    public static void set(Object o) {
        threadLocalMap.put(Thread.currentThread().getId(), o);
    }

    public static Object get() {
        return threadLocalMap.get(Thread.currentThread().getId());
    }
}

/**
 * 1) мапа не потокобезопасная (ну тут должен сам догадаться хотя бы до 1 варика):
 *      1. использовать concurrentHashMap
 *      2. сделать методы synchronized с текущей реализацией map
 * <p>
 * 2) Сделать текущий класс Дженериком и унаследовать от него ThreadLocalHeader
 * (можно намекнуть, что лушче бы избаваиться от object)
 * <p>
 * 3) нет очистки мапы, она будет забиваться всё больше с каждым запросом, нужен метод clear:
 * (можно намекнуть раскручивая тему со статикой, спросить: а когда GC будет проводить сборку?
 *  может ли GC удалить мапу? как очистить объекты внутри)
 * public static void clear() {
 *     threadLocalMap.remove(Thread.currentThread().getName());
 * }
 * 4) Топчик если чел понимает что в Tomcat фиксированное кол-во потоков и мапа в целом не разрастется,
 * т.к. пул потоков выделяется сразу и там фиксированные id и имена, таким образом если у нас 100 потоков с
 * одним именем, то новые значения буду просто перезаписываться) - ну это прям сеньор-помидор уже
 */
