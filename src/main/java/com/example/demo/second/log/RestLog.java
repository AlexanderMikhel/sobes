package com.example.demo.second.log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RestLog implements LogService {

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public void log() {
        //restTemplate.exchange();
    }

}
