package com.example.demo.second.model;

import lombok.Data;

import java.util.List;

@Data
public class Street {
    private List<Building> buildingList;
}
