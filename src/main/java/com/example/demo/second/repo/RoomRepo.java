package com.example.demo.second.repo;

import com.example.demo.second.model.Building;
import com.example.demo.second.model.Room;

import java.util.List;

public class RoomRepo implements Repo<Room> {
    @Override
    public Room get() {
        return null;
    }

    public List<Room> getByBuilding(Building building) {
        return List.of();
    }

    @Override
    public void add(Room room) {

    }

    @Override
    public void update(Room room) {

    }

    @Override
    public void remove(Long id) {

    }
}
