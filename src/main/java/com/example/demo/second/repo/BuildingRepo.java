package com.example.demo.second.repo;

import com.example.demo.second.model.Building;
import com.example.demo.second.model.Street;

import java.util.List;

public class BuildingRepo implements Repo<Building> {


    public List<Building> getByStreet(Street street) {
        return List.of();
    }

    @Override
    public Building get() {
        return null;
    }

    @Override
    public void add(Building building) {

    }

    @Override
    public void update(Building building) {

    }

    @Override
    public void remove(Long id) {

    }
}
