package com.example.demo.second.repo;

import com.example.demo.second.model.Street;

public class StreetRepo implements Repo<Street> {
    @Override
    public Street get() {
        return null;
    }

    public boolean checkStreetExists() {
        return true;
    }

    @Override
    public void add(Street street) {

    }

    @Override
    public void update(Street street) {

    }

    @Override
    public void remove(Long id) {

    }
}
