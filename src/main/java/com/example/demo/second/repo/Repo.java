package com.example.demo.second.repo;

public interface Repo<T> {

    T get();

    void add(T t);

    void update(T t);

    void remove(Long id);
}
