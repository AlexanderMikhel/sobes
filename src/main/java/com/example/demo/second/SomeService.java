package com.example.demo.second;

import com.example.demo.second.log.LogService;
import com.example.demo.second.model.Building;
import com.example.demo.second.model.Street;
import com.example.demo.second.repo.BuildingRepo;
import com.example.demo.second.repo.StreetRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class SomeService {

    private final StreetRepo streetRepo;
    private final BuildingRepo buildingRepo;
    private final LogService logService;

    public boolean checkStreetExists() {
        return streetRepo.checkStreetExists();
    }

    public Street get() {
        Street street = streetRepo.get();
        List<Building> buildingList = buildingRepo.getByStreet(street);
        street.setBuildingList(buildingList);
        return street;
    }

    public void add(Street street) {
        streetRepo.add(street);
        street.getBuildingList().forEach(buildingRepo::add);
        logToDataBase();
    }


    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void logToDataBase() {
        logService.log();
    }

}

/**
 * ЧАСТЬ 1, без учета папки log
 * 1) Transactional необходимо пометить только метод add (уточнить с хибером проблем не будет)
 * 2) в методе add необходима butch - вставка а не по одному (можно навести: что будет если билдингов много)
 * 3) приватные методы не могут быть помечены Transactional
 * 4) будет ли создана новая транзакция при вызове метода logToDataBase? - почему нет?) - как исправить?
 * ЧАСТЬ 2, с учетом папки log
 * 1) у нас изначально есть только DBLog, что будет если появится еще 1 наследник LogService? как это разрулить? (квалифаер, праймари)
 * 2) можно ли сделать код так, чтобы менять только проперти файл и тем самым подбирать нужный бин? (ConditionalOnProperty) или просто через value
 * 3) Попросить разлуить ситуацию если класс RestLog не фурычит (класть сообщения в БД) и почему @Autowired над полем подчеркнут идеей?
 */
