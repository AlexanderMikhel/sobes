package com.example.demo.first;

import com.example.demo.DemoApplication;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.stream.Stream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = DemoApplication.class)
@AutoConfigureMockMvc
class ThreadLocalControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    void someMethod() throws Exception {
        Stream.iterate(0, n-> n+1)
                .limit(1000)
                .parallel()
                .forEach(n -> {
                    try {
                        String s = String.valueOf(n);
                        ResultActions res = mvc.perform(get("http://localhost:8080/someMethod")
                                .contentType(MediaType.APPLICATION_JSON)
                                .header("uncId", s)
                                .header("requestId", s)
                                .header("userId", s))
                                .andExpect(status().isOk());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
        System.out.println(ThreadLocalCustom.get());
    }
}